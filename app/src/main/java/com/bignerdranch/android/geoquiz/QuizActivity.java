package com.bignerdranch.android.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG="QuizActivity";
    private static final String KEY_INDEX="index";
    private static final String KEY_CHEATED="cheated";
    private static final int REQUEST_CODE_CHEAT=0;

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mBackButton;
    private TextView mQuestionTextView;
    //
    private Button mCheatButton;

    private Question[] mQuestionBank=new Question[]{
            new Question(R.string.question_turkey,false),
            new Question(R.string.question_oceans,true),
            new Question(R.string.question_mideast,false),
            new Question(R.string.question_africa,false),
            new Question(R.string.question_americas,true),
            new Question(R.string.question_asia,true)
    };

    private int mCurrentIndex=0;
    private boolean mIscheater;

    private void updateQuestion(){
        int question=mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
        mIscheater=mQuestionBank[mCurrentIndex].isHasCheated();
    }

    private void checkAnswer(boolean userPressedTrue){
        boolean answerIsTrue=mQuestionBank[mCurrentIndex].isAnswerTrue();
        int messageResId=0;
        if(mIscheater){
            messageResId=R.string.judgment_toast;
        }else {
            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
            } else {
                messageResId = R.string.incorrect_toast;
            }
        }

        Toast.makeText(this,messageResId,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        //textQuestion
        mQuestionTextView=(TextView)findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mCurrentIndex=(mCurrentIndex+1)%mQuestionBank.length;
                updateQuestion();
            }
        });
        //gettting view's buttons reference
        mTrueButton=(Button)findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                checkAnswer(true);
            }
        });

        mFalseButton=(Button)findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                checkAnswer(false);
            }
        });
        //backButton
        mBackButton=(ImageButton)findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mCurrentIndex=mCurrentIndex-1;
                if(mCurrentIndex<0){
                    mCurrentIndex=mQuestionBank.length-1;
                }
                updateQuestion();
            }
        });

        //NexButton
        mNextButton=(ImageButton)findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mCurrentIndex=(mCurrentIndex+1)%mQuestionBank.length;
                updateQuestion();
            }
        });

        //Cheat Button
        mCheatButton=(Button)findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                boolean answerIsTrue=mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent i=CheatActivity.newIntent(QuizActivity.this,answerIsTrue);
                startActivityForResult(i,REQUEST_CODE_CHEAT);
            }
        });

        //check SavedIndex
        if(savedInstanceState!=null){
            mCurrentIndex=savedInstanceState.getInt(KEY_INDEX,0);
            convertBooleanArrayToQuestionArray(savedInstanceState.getBooleanArray(KEY_CHEATED));
        }

        updateQuestion();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstaceState){
        super.onSaveInstanceState(savedInstaceState);
        Log.d(TAG,"onSaveInstanceState");
        savedInstaceState.putInt(KEY_INDEX,mCurrentIndex);
        savedInstaceState.putBooleanArray(KEY_CHEATED,convertQuestionArrayToBooleanArray());
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        Log.d(TAG,"onActivityResult");
        if(resultCode!= Activity.RESULT_OK){
            return;
        }

        if(requestCode==REQUEST_CODE_CHEAT){
            if(data==null){
                return;
            }
            mIscheater=CheatActivity.wasAnswerShown(data);
            mQuestionBank[mCurrentIndex].setHasCheated(mIscheater);
        }
    }

    private boolean[] convertQuestionArrayToBooleanArray(){
        boolean[] cheaterArray=new boolean[mQuestionBank.length];
        for(int i=0;i<cheaterArray.length;i++){
            cheaterArray[i]=mQuestionBank[i].isHasCheated();
        }

        return cheaterArray;
    }

    private void convertBooleanArrayToQuestionArray(boolean[] cheaterArray) {
        for(int i=0;i<cheaterArray.length;i++){
            mQuestionBank[i].setHasCheated(cheaterArray[i]);
        }
    }

        @Override
    public void onStart(){
        super.onStart();
        Log.d(TAG,"onStart() called");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG,"onResume() called");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d(TAG,"onPause() called");
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d(TAG,"onStop() called");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG,"onDestroy() called");
    }
}
